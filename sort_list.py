
def get_three_higest_number_in_the_list(list_of_numbers):
    sorted_list = sorted(list_of_numbers)
    return sorted_list[-3:]

if __name__ == '__main__':
    print(get_three_higest_number_in_the_list([11,5,3,-14]))
    print(get_three_higest_number_in_the_list([25,0.0,66,"ptak"]))