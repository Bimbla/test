def hello_person_from_city(name, city):
    return f"Hello {name} from {city}"

if __name__ == '__main__':
    print(hello_person_from_city("Magda","Wroclaw"))
    print(hello_person_from_city("Anna","Opole"))