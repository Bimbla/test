def check_weaver_conditions(temp_in_celsius, pressuer_in_hpa):
    if temp_in_celsius == 0 and pressuer_in_hpa == 1013:
        return True
    else:
        return False


def calculate_grade_for_test_score(points):
    if points > 90:
        print("5"),
    elif points > 75:
        print("4"),
    elif points > 50:
        print("3"),
    else:
        print(2)


def calculate_grade_for_test_score_2(points):
    if points > 90:
        return 5
    elif points > 75:
        return 4
    elif points > 50:
        return 3
    else:
        return 2


if __name__ == '__main__':
    print(calculate_grade_for_test_score(13))
    print(calculate_grade_for_test_score_2(71))
    print(check_weaver_conditions(0, 1013))
