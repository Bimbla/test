animal = {
    "kind": "dog",
    "age": 1,
    "male": True
}
animal2 = {
    "kind": "cat",
    "age": 2,
    "male": True
}

animals = [

    {
        "kind": "dog",
        "age": 1,
        "male": True
    },
    {
        "kind": "cat",
        "age": 2,
        "male": True
    },
    {
        "kind": "fish",
        "age": 0.5,
        "male": True
    },
]
# dlugość
print(len(animal))
# obiekt nr 1
print(animals[0])
last_animal = animals[-1]
print(last_animal)
last_animal_age = last_animal["age"]
print(last_animal_age)
print('The age of last animal is equal to ', last_animal_age)
print(animals[0]["male"])

animals.append(
    {
        "kind": "bird",
        "age": 10,
        "male": True
    },
)
print(animals[0]["age"])
print(animals[1]["kind"])
