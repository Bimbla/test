def add_two_numers(a,b):
    return a + b

if __name__ == '__main__':
    print(add_two_numers("kot ","pies" ))
    print(add_two_numers(7,12))
    print(add_two_numers(True,False))
    print(add_two_numers(False, False))
    ## poniiżej True i True sa traktowane jak liczba
    print(add_two_numers(True,True))
    print(add_two_numers(True,7))
    print(add_two_numers(False, 7))

##funkcja musi mieć return lub print żeby cos zwracac