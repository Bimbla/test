##Uzywane do generowania damych do ładaoania
##Uses to generate date to load

## Funkcja ma argumenty będące listą potrzebnych danych
## zwraca dane ustrukturyzowane jako lista
## na liście można robić kolejne operacje
## jest tutaj lista w liscie
def prepare_user_payload(email, phone, city, street):

    return {

        'contact': {

            'email': email,

            'phone': phone

        },

        'address': {

            'city': city,

            'street': street

        }

    }
if __name__ == '__main__':
    payload=prepare_user_payload("aaa@exaple.com" , "123456799", "wrocław", "Prosta")
    print(payload)